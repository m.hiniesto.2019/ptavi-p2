#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import unittest
import computecsv

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_operations = """Default: 3.0
8.0
16.0
3.0
2.0
Bad format
Bad format
8.0
Operations: 5
"""


class TestComputeCount(unittest.TestCase):

    def test_csv(self):
        stdout = StringIO()
        with contextlib.redirect_stdout(stdout):
            computecsv.process_csv(os.path.join(parent_dir, 'operations.csv'))
        output = stdout.getvalue()
        self.assertEqual(output, result_operations)


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
