

import math

class Compute():


    def __init__(self, num, exporbase):
        self.default = 2
        self.count = 0
        self.num = num
        self.exporbase = exporbase
    def power(self):
        self.count += 1
        return self.num ** self.exporbase
    def log(self):
        self.count += 1
        return math.log(self.num, self.exporbase)
    def set_def(self):
        self.count += 1
        if self.exporbase == None:
            return(self.default)
        if self.exporbase <= 0:
            raise ValueError("third argumnet should be <= 0")
    def get_def(self):
       self.count += 1
       return(self.default)

