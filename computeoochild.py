import sys
import computeoo
import math


class ComputeooChild(computeoo.Compute):

    def __init__(self, num, num2):
        self.default = 2
        self.num = num
        self.exporbase = num2
        if num2 == None:
            num2 == self.default
    def set_def(self):
        if self.exporbase == None:
            return(self.default)
        if self.exporbase <= 0:
            raise ValueError("third argumnet should be >= 0")
    def get_def(self):
        print(self.exporbase)
    def power(self):
            return self.num ** self.exporbase

    def log(self):
            return math.log(self.num, self.exporbase)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        num = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        num2 = None
    if len(sys.argv) == 4:
        try:
         num2 = float(sys.argv[3])
        except ValueError:
         sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        objeto = ComputeooChild(num, num2)
        objeto.set_def()
        result = objeto.power()
    elif sys.argv[1] == "log":
        objeto = ComputeooChild(num, num2)
        objeto.set_def()
        result = objeto.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)